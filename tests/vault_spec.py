import os
from mockito import when, mock, unstub
from expects import *
from mamba import description, context, it
import vault
import tests.vars as vars


# with description("Vault"):
#     with before.each:
#         unstub()
#         self._vault = vault.Vault(
#             vars.host_url,
#             vars.role_id,
#             vars.secret_id,
#             vars._print)

#     with context("__init__"):
#         with it("should set _print"):
#             expect(self._vault._print).to(equal(vars._print))

#     with context("list"):
#         with it("should list all secret"):
#             secret = self._vault.v2.list('denis', 'tot')
#             expect('request_id' in secret).to(equal(True))

#     with context("read"):
#         with it("should read a secret"):
#             secret = self._vault.v2.read('denis', 'tot/default secret')
#             expect(isinstance(secret['data']['data'], dict)).to(equal(True))

#     with context("write"):
#         with it("should write a secret"):
#             secret = self._vault.v2.write('denis', 'tot/abc', {'aa': 'bb'})
#             expect('created_time' in secret['data']).to(equal(True))

#     with context("delete"):
#         with it("should delete a secret"):
#             secret = self._vault.v2.delete('denis', 'tot/abc')
#             expect(secret.status_code).to(equal(204))
