from setuptools import setup, find_packages
from vault.__version__ import (
    __version__,
)
with open('requirements.txt') as f:
    required = f.read().splitlines()

setup(name='cnd_vault',
    version=__version__,
    description="Tools to read/write secret",
    long_description=open('README.md').read(),
    long_description_content_type='text/markdown',
    classifiers=[
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent'
    ],
    keywords='',
    author='Denis FABIEN',
    author_email='denis.fabien@changendevops.com',
    url='https://gitlab.com/changendevops/cnd-vault',
    license='MIT/X11',
    packages=find_packages(exclude=['ez_setup']),
    include_package_data=True,
    package_data={'vault': ['VERSION'], 'Project': ['*']},
    zip_safe=False,
    install_requires=required,
    test_require=['expect', 'doublex', 'doublex-expects'],
    project_urls={
        "Documentation": "https://changendevops.com",
        "Source": "https://gitlab.com/changendevops/cnd-vault",
    },
)
